FROM openjdk:8-slim
COPY /target/ride-app-1.0-SNAPSHOT.jar /app/application.jar
#COPY /build/resources /app
ENTRYPOINT ["java","-jar","/app/application.jar"]