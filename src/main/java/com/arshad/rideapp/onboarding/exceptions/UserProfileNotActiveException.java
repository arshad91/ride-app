package com.arshad.rideapp.onboarding.exceptions;

public class UserProfileNotActiveException extends Exception{

    public UserProfileNotActiveException(String message) {
        super(message);
    }
}
