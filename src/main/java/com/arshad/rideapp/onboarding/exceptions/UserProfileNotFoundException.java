package com.arshad.rideapp.onboarding.exceptions;

public class UserProfileNotFoundException extends Exception{

    public UserProfileNotFoundException(String message) {
        super(message);
    }
}
