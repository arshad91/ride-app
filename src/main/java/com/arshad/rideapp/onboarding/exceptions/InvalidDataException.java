package com.arshad.rideapp.onboarding.exceptions;

public class InvalidDataException extends Exception{

    public InvalidDataException(String message) {
        super(message);
    }
}
