package com.arshad.rideapp.onboarding.dto.request;

import com.arshad.rideapp.onboarding.dto.response.BaseResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentRequestDTO extends BaseResponseDTO {
    private Long id;
    private String fileName;
    private String contentType;
    private LocalDateTime uploadDate;
}
