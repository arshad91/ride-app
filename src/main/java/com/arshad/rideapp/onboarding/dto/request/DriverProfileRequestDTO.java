package com.arshad.rideapp.onboarding.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DriverProfileRequestDTO {
    private String vehicleType;
    private String licensePlate;
}
