package com.arshad.rideapp.onboarding.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserResponseDTO extends BaseResponseDTO{
    private String name;
    private String email;
    private String phoneNumber;
}
