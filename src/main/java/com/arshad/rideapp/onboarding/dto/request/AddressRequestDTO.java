package com.arshad.rideapp.onboarding.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddressRequestDTO {
    private String flatno;
    private String street;
    private String pincode;
    private String city;
    private String state;
    private String country;
}
