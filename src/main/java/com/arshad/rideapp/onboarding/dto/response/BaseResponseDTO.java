package com.arshad.rideapp.onboarding.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BaseResponseDTO {
    private Long id;
    private int statusCode;
    private String message;
}
