package com.arshad.rideapp.onboarding.businesspolicy;

import com.arshad.rideapp.onboarding.dto.request.DriverProfileRequestDTO;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DriverProfileBusinessPolicy implements IBusinessPolicy<DriverProfileRequestDTO> {
    @Override
    public void validate(DriverProfileRequestDTO driverProfileRequestDTO) throws InvalidDataException {
        log.info("Driver profile details validation started");
        validateVehicleType(driverProfileRequestDTO.getVehicleType());
        validateLicensePlate(driverProfileRequestDTO.getLicensePlate());
        log.info("Driver profile details validated");
    }

    private void validateVehicleType(String vehicleType) throws InvalidDataException {
        if (vehicleType.isEmpty()) throw new InvalidDataException("VehicleType " + vehicleType + " is invalid");
    }

    private void validateLicensePlate(String licensePlate) throws InvalidDataException {
        if (licensePlate.isEmpty()) throw new InvalidDataException("LicensePlate " + licensePlate + " is invalid");
    }
}
