package com.arshad.rideapp.onboarding.businesspolicy;

import com.arshad.rideapp.onboarding.dto.request.DocumentRequestDTO;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DocumentBusinessPolicy implements IBusinessPolicy<DocumentRequestDTO> {
    @Override
    public void validate(DocumentRequestDTO documentRequestDTO) throws InvalidDataException {
        log.info("Document details validation started");
        validateFileName(documentRequestDTO.getFileName());
        validateContentType(documentRequestDTO.getContentType());
        log.info("Document details validated");
    }

    private void validateContentType(String contentType) throws InvalidDataException {
    }

    private void validateFileName(String fileName) throws InvalidDataException {
    }

}
