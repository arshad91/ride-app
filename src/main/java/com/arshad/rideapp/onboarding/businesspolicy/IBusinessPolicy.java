package com.arshad.rideapp.onboarding.businesspolicy;

public interface IBusinessPolicy<T> {

    void validate(T t) throws Exception;

}
