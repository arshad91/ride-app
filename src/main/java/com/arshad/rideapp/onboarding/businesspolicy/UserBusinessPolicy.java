package com.arshad.rideapp.onboarding.businesspolicy;

import com.arshad.rideapp.onboarding.dto.request.AddressRequestDTO;
import com.arshad.rideapp.onboarding.dto.request.UserRegistrationDTO;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Matcher;

import static com.arshad.rideapp.onboarding.constants.Constants.emailRegex;
import static com.arshad.rideapp.onboarding.constants.Constants.mobileRegex;

@Slf4j
public class UserBusinessPolicy implements IBusinessPolicy<UserRegistrationDTO> {
    @Override
    public void validate(UserRegistrationDTO userDTO) throws InvalidDataException {
        log.info("Driver details validation started");
        validateName(userDTO.getName());
        validateEmail(userDTO.getEmail());
        validatePhone(userDTO.getPhoneNumber());
        validateAddress(userDTO.getAddress());
        log.info("Driver details validated");
    }

    private void validateName(String name) throws InvalidDataException {
        if (name == null || "".equals(name) || name.length() < 3) {
            throw new InvalidDataException("Name " + name + " is invalid");
        }
    }

    private void validateEmail(String email) throws InvalidDataException {
        Matcher matcher = emailRegex.matcher(email);
        if (!matcher.matches()) {
            throw new InvalidDataException("email " + email + " is invalid");
        }
    }

    private void validatePhone(String phoneNumber) throws InvalidDataException {
        Matcher matcher = mobileRegex.matcher(phoneNumber);
        if (!matcher.matches()) {
            throw new InvalidDataException("phoneNumber " + phoneNumber + " is invalid");
        }
    }

    private void validateAddress(AddressRequestDTO address) {

    }
}
