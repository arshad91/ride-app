package com.arshad.rideapp.onboarding.service;

import com.arshad.rideapp.onboarding.businesspolicy.DriverProfileBusinessPolicy;
import com.arshad.rideapp.onboarding.dto.request.DriverProfileRequestDTO;
import com.arshad.rideapp.onboarding.dto.response.DriverProfileResponseDTO;
import com.arshad.rideapp.onboarding.entity.DriverProfile;
import com.arshad.rideapp.onboarding.enums.Status;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.exceptions.UserProfileNotActiveException;
import com.arshad.rideapp.onboarding.exceptions.UserProfileNotFoundException;
import com.arshad.rideapp.onboarding.mapper.DriverProfileMapper;
import com.arshad.rideapp.onboarding.repository.DriverProfileRepository;
import com.arshad.rideapp.onboarding.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
public class DriverProfileService extends DriverProfileBusinessPolicy {
    @Autowired
    private DriverProfileRepository driverProfileRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DriverProfileMapper driverProfileMapper;

    public DriverProfileResponseDTO createDriverProfile(Long userId, DriverProfileRequestDTO driverProfileRequestDTO) throws InvalidDataException, UserNotFoundException, UserAlreadyExistsException {
        log.info("Inside updateDriverProfile");
        // validate data
        super.validate(driverProfileRequestDTO);

        validateUser(userId);

        DriverProfile driverProfile = driverProfileMapper.mapDriverProfileDTOToDomain(driverProfileRequestDTO, userId);
        LocalDateTime currentDateTime = LocalDateTime.now();
        driverProfile.setCreatedDate(currentDateTime);
        driverProfile.setUpdatedDate(currentDateTime);

        // save to the database.
        DriverProfile driverProfileResponse = driverProfileRepository.save(driverProfile);
        log.info("Drive profile created successfully");
        return driverProfileMapper.mapDriverProfileDomainToDTO(driverProfileResponse);
    }

    public DriverProfileResponseDTO updateDriverProfile(Long userId, DriverProfileRequestDTO driverProfileRequestDTO) throws InvalidDataException, UserNotFoundException, UserAlreadyExistsException, UserProfileNotFoundException {
        log.info("Inside updateDriverProfile");
        // validate data
        super.validate(driverProfileRequestDTO);

        userRepository.findById(userId).orElseThrow(()-> new UserNotFoundException("User not found"));
        log.info("UserId :{} found ", userId);

        DriverProfile driverProfile = driverProfileMapper.mapDriverProfileDTOToDomain(driverProfileRequestDTO, userId);
        Optional<DriverProfile> optionalDriverProfile = driverProfileRepository.findByUserId(userId);

        if(optionalDriverProfile.isPresent()) {
            DriverProfile existingDriverProfile = optionalDriverProfile.get();
            existingDriverProfile.setVehicleType(driverProfile.getVehicleType());
            existingDriverProfile.setLicensePlate(driverProfile.getLicensePlate());
            existingDriverProfile.setUpdatedDate(LocalDateTime.now());

            // save to the database.
            DriverProfile driverProfileResponse = driverProfileRepository.save(existingDriverProfile);
            log.info("Drive profile updated successfully");
            return driverProfileMapper.mapDriverProfileDomainToDTO(driverProfileResponse);
        } else {
            throw new UserProfileNotFoundException("User profile does not exists");
        }
    }

    public String updateDriverStatus(Long driverId, Status status) throws UserNotFoundException, UserProfileNotActiveException {
        log.info("Inside updateDriverStatus");
        log.info("Fetching driver profile details for driverId: {}", driverId);
        Optional<DriverProfile> optionalProfile = driverProfileRepository.findByUserId(driverId);
        log.info("Completed fetching driver profile details for driverId: {}", driverId);

        if (optionalProfile.isPresent()) {
            log.info("Driver details found for driverId: {}", driverId);
            DriverProfile driverProfile = optionalProfile.get();
            validateStatus(status, driverProfile.getDriverStatus());
            driverProfile.setDriverStatus(status.name());
            driverProfile.setUpdatedDate(LocalDateTime.now());
            driverProfileRepository.save(driverProfile);
            log.info("Driver status updated to : {} for driverId: {}", status.name(), driverId);
            return "Driver status "+status.name()+" updated successfully";
        }
        else {
            log.info("Driver profile details not found for driverId: {}", driverId);
            throw new UserNotFoundException("Driver profile not found");
        }
    }

    /**
     * If driver profile status is not active and we try to make driver available to take ride then we should throw exception
     * @param status
     * @param dbStatus
     * @throws UserProfileNotActiveException
     */
    private void validateStatus(Status status, String dbStatus) throws UserProfileNotActiveException {
        if (Status.AVAILABLE.equals(status) && !Status.ACTIVE.name().equalsIgnoreCase(dbStatus))
            throw new UserProfileNotActiveException("Driver profile is not active");
    }

    private void validateUser(Long userId) throws UserNotFoundException, UserAlreadyExistsException {
        // check if User / driver exists
        userRepository.findById(userId).orElseThrow(()-> new UserNotFoundException("User not found"));
        log.info("UserId :{} found ", userId);

        //check if profile already created
        if(driverProfileRepository.findByUserId(userId).isPresent()) {
            throw new UserAlreadyExistsException("Driver profile already exists for userId : " + userId);
        }
    }
}
