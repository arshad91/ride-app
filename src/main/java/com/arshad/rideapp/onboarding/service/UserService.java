package com.arshad.rideapp.onboarding.service;


import com.arshad.rideapp.onboarding.businesspolicy.UserBusinessPolicy;
import com.arshad.rideapp.onboarding.dto.request.UserRegistrationDTO;
import com.arshad.rideapp.onboarding.dto.response.UserResponseDTO;
import com.arshad.rideapp.onboarding.entity.Address;
import com.arshad.rideapp.onboarding.entity.User;
import com.arshad.rideapp.onboarding.enums.Status;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.mapper.AddressMapper;
import com.arshad.rideapp.onboarding.mapper.UserMapper;
import com.arshad.rideapp.onboarding.repository.AddressRepository;
import com.arshad.rideapp.onboarding.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class UserService extends UserBusinessPolicy {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AddressRepository addressRepository;

    public UserResponseDTO registerUser(UserRegistrationDTO userRegistrationDTO) throws InvalidDataException, UserAlreadyExistsException {
        log.info("Inside create user");
        // validate data,
        super.validate(userRegistrationDTO);

        if (isEmailAlreadyExists(userRegistrationDTO.getEmail())) {
            log.error("Email already exists: {}", userRegistrationDTO.getEmail());
            throw new UserAlreadyExistsException(userRegistrationDTO.getEmail() + " already exists");
        }

        // Perform registration logic
        User user = userMapper.mapUserDTOToDomain(userRegistrationDTO);
        LocalDateTime currentDateTime = LocalDateTime.now();
        user.setCreatedDate(currentDateTime);
        user.setUpdatedDate(currentDateTime);
        System.out.println("currentDateTime= " + user.getCreatedDate());
        log.info("currentDateTime= " + user.getCreatedDate());
        // save to the database.
        User responseDriver = userRepository.save(user);

        Address address = addressMapper.mapAddressRequestDTOToDomain(userRegistrationDTO.getAddress(), user);
        address.setCreatedDate(currentDateTime);
        address.setUpdatedDate(currentDateTime);
        // save to the database.
        Address responseAddress = addressRepository.save(address);

        log.info("Driver Created successfully");
        return userMapper.mapUserDomainToDTO(responseDriver);
    }

    private boolean isEmailAlreadyExists(String email) {
        log.info("Fetching details for user: {}", email);
        Optional<User> optionalUser = userRepository.findByEmail(email);
        log.info("Fetching details for user: {}", email);
        return optionalUser.isPresent();
    }

    public Optional<User> getUser(String email) throws UserNotFoundException {
        return Optional.ofNullable(userRepository.findByEmail(email).orElseThrow(() -> new UserNotFoundException("Driver not found")));
    }

    public Optional<List<User>> findAllUsers(String name) throws UserNotFoundException {
        return Optional.ofNullable(userRepository.findByName(name).orElseThrow(() -> new UserNotFoundException("Driver not found")));
    }

}

