package com.arshad.rideapp.onboarding.service;

import com.arshad.rideapp.onboarding.businesspolicy.DocumentBusinessPolicy;
import com.arshad.rideapp.onboarding.dto.request.DocumentRequestDTO;
import com.arshad.rideapp.onboarding.dto.response.DocumentResponseDTO;
import com.arshad.rideapp.onboarding.entity.Document;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.mapper.DocumentMapper;
import com.arshad.rideapp.onboarding.repository.DocumentRepository;
import com.arshad.rideapp.onboarding.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
@Slf4j
public class DocumentService extends DocumentBusinessPolicy {
    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DocumentMapper documentMapper;

    public DocumentResponseDTO uploadDocument(Long userId, MultipartFile file) throws IOException, InvalidDataException, UserNotFoundException {
        log.info("Inside uploadDocument");
        DocumentRequestDTO documentRequestDTO = prepareDocumentRequest(file);
        // validate data,
        super.validate(documentRequestDTO);
        validateUser(userId);

        Document document = documentMapper.mapDocumentDTOToDomain(documentRequestDTO, userId);
        byte[] fileBytes = file.getBytes();

        // store the file in a cloud service like AWS S3
        document.setFileLocation(uploadDocumentToS3(userId, file));
        document.setDocument(fileBytes);
        Document documentResponse = documentRepository.save(document);
        log.info("Document uploaded successfully for userId : {}", userId);
        initiateBackgroundCheck(userId, documentResponse);
        return documentMapper.mapDocumentDomainToDTO(documentResponse);
    }

    /**
     *  upload the file to AWS S3
     * @param userId
     * @param file
     * @return S3 location
     */
    private String uploadDocumentToS3(Long userId, MultipartFile file) {
        return "S3://bucket/" + userId + "/" + file.getOriginalFilename();
    }

    private void initiateBackgroundCheck(Long userId, Document documentResponse) {
        //call third party API/publish message to kafka for background verification
        // publish message to Kafka which would change driver profile status to BGC_IN_PROGRESS
    }

    private DocumentRequestDTO prepareDocumentRequest(MultipartFile file) {
        DocumentRequestDTO documentRequestDTO = new DocumentRequestDTO();
        documentRequestDTO.setFileName(file.getOriginalFilename());
        documentRequestDTO.setContentType(file.getContentType());
        return documentRequestDTO;
    }

    private void validateUser(Long userId) throws UserNotFoundException {
        // check if User/driver exists
        userRepository.findById(userId).orElseThrow(()-> new UserNotFoundException("User not found"));
        log.info("UserId :{} found ", userId);
    }
}
