package com.arshad.rideapp.onboarding.enums;

public enum Status {
    PENDING,
    BLOCKED,
    ACTIVE,
    AVAILABLE,
    INACTIVE,
    BGC_IN_PROGRESS,
    RIDING
}
