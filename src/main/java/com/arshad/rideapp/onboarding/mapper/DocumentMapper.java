package com.arshad.rideapp.onboarding.mapper;

import com.arshad.rideapp.onboarding.dto.request.DocumentRequestDTO;
import com.arshad.rideapp.onboarding.dto.response.DocumentResponseDTO;
import com.arshad.rideapp.onboarding.entity.Document;
import com.arshad.rideapp.onboarding.entity.User;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class DocumentMapper {

    public Document mapDocumentDTOToDomain(DocumentRequestDTO documentRequestDTO, Long userId) {
        Document document = new Document();
        document.setFileName(documentRequestDTO.getFileName());
        document.setContentType(documentRequestDTO.getContentType());
        document.setUploadDate(LocalDateTime.now());
        User user = new User();
        user.setId(userId);
        document.setUser(user);
        return document;
    }

    public DocumentResponseDTO mapDocumentDomainToDTO(Document document) {
        DocumentResponseDTO responseDTO = new DocumentResponseDTO();
        responseDTO.setId(document.getId());
        responseDTO.setFileName(document.getFileName());
        responseDTO.setContentType(document.getContentType());
        return responseDTO;
    }
}
