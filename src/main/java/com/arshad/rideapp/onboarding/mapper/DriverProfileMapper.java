package com.arshad.rideapp.onboarding.mapper;

import com.arshad.rideapp.onboarding.dto.request.DriverProfileRequestDTO;
import com.arshad.rideapp.onboarding.dto.response.DriverProfileResponseDTO;
import com.arshad.rideapp.onboarding.entity.DriverProfile;
import com.arshad.rideapp.onboarding.entity.User;
import com.arshad.rideapp.onboarding.enums.Status;
import org.springframework.stereotype.Component;

@Component
public class DriverProfileMapper {

    public DriverProfile mapDriverProfileDTOToDomain(DriverProfileRequestDTO driverProfileRequestDTO, Long userId) {
        DriverProfile driverProfile = new DriverProfile();
        User user = new User();
        user.setId(userId);

        driverProfile.setUser(user);
        driverProfile.setVehicleType(driverProfileRequestDTO.getVehicleType());
        driverProfile.setLicensePlate(driverProfileRequestDTO.getLicensePlate());
        driverProfile.setRating(new Double(0));
        driverProfile.setDriverStatus(Status.PENDING.name());
        return driverProfile;
    }

    public DriverProfileResponseDTO mapDriverProfileDomainToDTO(DriverProfile driverProfile) {
        DriverProfileResponseDTO driverProfileResponseDTO = new DriverProfileResponseDTO();
        driverProfileResponseDTO.setId(driverProfile.getUser() != null ? driverProfile.getUser().getId() : null);
        driverProfileResponseDTO.setVehicleType(driverProfile.getVehicleType());
        driverProfileResponseDTO.setLicensePlate(driverProfile.getLicensePlate());
        return driverProfileResponseDTO;
    }

}
