package com.arshad.rideapp.onboarding.mapper;

import com.arshad.rideapp.onboarding.dto.request.AddressRequestDTO;
import com.arshad.rideapp.onboarding.entity.Address;
import com.arshad.rideapp.onboarding.entity.User;
import org.springframework.stereotype.Component;

@Component
public class AddressMapper {

    public Address mapAddressRequestDTOToDomain(AddressRequestDTO requestDTO, User user) {
        Address address = new Address();
        address.setUser(user);
        address.setFlatno(requestDTO.getFlatno());
        address.setStreet(requestDTO.getStreet());
        address.setPincode(requestDTO.getPincode());
        address.setCity(requestDTO.getCity());
        address.setState(requestDTO.getState());
        address.setCountry(requestDTO.getCountry());
        return address;
    }

}
