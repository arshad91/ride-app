package com.arshad.rideapp.onboarding.mapper;

import com.arshad.rideapp.onboarding.dto.request.UserRegistrationDTO;
import com.arshad.rideapp.onboarding.dto.response.UserResponseDTO;
import com.arshad.rideapp.onboarding.entity.User;
import com.arshad.rideapp.onboarding.enums.Status;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Base64;

@Component
public class UserMapper {

    public User mapUserDTOToDomain(UserRegistrationDTO userRegistrationDTO) {
        User user = new User();
        user.setName(userRegistrationDTO.getName());
        user.setEmail(userRegistrationDTO.getEmail());
        user.setPassword(Base64.getEncoder().withoutPadding().encodeToString(userRegistrationDTO.getPassword().getBytes()));
        user.setPhoneNumber(userRegistrationDTO.getPhoneNumber());
        user.setStatus(Status.PENDING.name());
        return user;
    }

    public UserResponseDTO mapUserDomainToDTO(User user) {
        UserResponseDTO userResponseDTO = new UserResponseDTO();
        userResponseDTO.setId(user.getId());
        userResponseDTO.setName(user.getName());
        userResponseDTO.setEmail(user.getEmail());
        userResponseDTO.setPhoneNumber(user.getPhoneNumber());
        return userResponseDTO;
    }
}
