package com.arshad.rideapp.onboarding.controller;

import com.arshad.rideapp.onboarding.dto.request.UserRegistrationDTO;
import com.arshad.rideapp.onboarding.dto.response.UserResponseDTO;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@RequestMapping("/v1/user")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public ResponseEntity<UserResponseDTO> registerUser(@RequestBody UserRegistrationDTO userRegistrationDTO) {
        try {
            UserResponseDTO responseDTO = userService.registerUser(userRegistrationDTO);
            responseDTO.setStatusCode(HttpStatus.CREATED.value());
            responseDTO.setMessage("User registered successfully.");
            return ResponseEntity.created(new URI("/register")).body(responseDTO);
        } catch (InvalidDataException e) {
            log.error("InvalidDataException occurred:{}", e);
            return buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }  catch (UserAlreadyExistsException e) {
            log.error("UserAlreadyExistsException occurred:{}", e);
            return buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }  catch (Exception e) {
            log.error("Exception occurred:{}", e.getMessage());
            return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Exception occurred in User registration");
        }
    }

    private ResponseEntity<UserResponseDTO> buildErrorResponse(HttpStatus httpStatus, String errorMessage) {
        UserResponseDTO responseDTO = new UserResponseDTO();
        responseDTO.setMessage(errorMessage);
        responseDTO.setStatusCode(httpStatus.value());
        return ResponseEntity.status(httpStatus).body(responseDTO);
    }

}

