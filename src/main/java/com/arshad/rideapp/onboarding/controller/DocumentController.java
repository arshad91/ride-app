package com.arshad.rideapp.onboarding.controller;

import com.arshad.rideapp.onboarding.dto.response.DocumentResponseDTO;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.service.DocumentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;

@RestController
@RequestMapping("/v1/user")
@Slf4j
public class DocumentController {
    @Autowired
    private DocumentService documentService;

    @PostMapping("/{userId}/document/upload")
    public ResponseEntity<DocumentResponseDTO> uploadDocument(@PathVariable Long userId,
                                                              @RequestParam("file") MultipartFile file) {
        try {
            DocumentResponseDTO responseDTO = documentService.uploadDocument(userId, file);
            responseDTO.setStatusCode(HttpStatus.CREATED.value());
            responseDTO.setMessage("Document uploaded successfully. documentID: " + responseDTO.getId());
            return ResponseEntity.created(new URI("/register")).body(responseDTO);
        } catch (InvalidDataException e) {
            log.error("InvalidDataException occurred:{}", e);
            return buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UserNotFoundException e) {
            log.error("Exception occurred: {}", e.getMessage());
            return buildErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            log.error("Exception occurred:{}", e.getMessage());
            return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Failed to upload document");
        }
    }

    private ResponseEntity<DocumentResponseDTO> buildErrorResponse(HttpStatus httpStatus, String errorMessage) {
        DocumentResponseDTO responseDTO = new DocumentResponseDTO();
        responseDTO.setMessage(errorMessage);
        responseDTO.setStatusCode(httpStatus.value());
        return ResponseEntity.status(httpStatus).body(responseDTO);
    }

}
