package com.arshad.rideapp.onboarding.controller;

import com.arshad.rideapp.onboarding.dto.request.DriverProfileRequestDTO;
import com.arshad.rideapp.onboarding.dto.response.DriverProfileResponseDTO;
import com.arshad.rideapp.onboarding.enums.Status;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.exceptions.UserProfileNotActiveException;
import com.arshad.rideapp.onboarding.exceptions.UserProfileNotFoundException;
import com.arshad.rideapp.onboarding.service.DriverProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;


@RestController
@RequestMapping("/v1/driver/profile")
@Slf4j
public class DriverProfileController {
    @Autowired
    private DriverProfileService driverProfileService;

    @PostMapping("/{driverId}/register")
    public ResponseEntity<DriverProfileResponseDTO> createDriverProfile(@PathVariable Long driverId, @RequestBody DriverProfileRequestDTO driverProfileRequestDTO) {
        try {
            DriverProfileResponseDTO responseDTO = driverProfileService.createDriverProfile(driverId, driverProfileRequestDTO);
            responseDTO.setStatusCode(HttpStatus.CREATED.value());
            responseDTO.setMessage("Driver profile created successfully.");
            return ResponseEntity.created(new URI("/register")).body(responseDTO);
        } catch (UserNotFoundException e) {
            log.error("Exception occurred: {}", e.getMessage());
            return buildErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidDataException e) {
            log.error("InvalidDataException occurred:{}", e);
            return buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UserAlreadyExistsException e) {
            log.error("Exception occurred: {}", e.getMessage());
            return buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }catch (Exception e) {
            log.error("Exception occurred:{}", e.getMessage());
            return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Exception occurred in driver profile registration");
        }
    }


    @PutMapping("/{driverId}/update")
    public ResponseEntity<DriverProfileResponseDTO> updateDriverProfile(@PathVariable Long driverId, @RequestBody DriverProfileRequestDTO driverProfileRequestDTO) {
        try {
            DriverProfileResponseDTO responseDTO = driverProfileService.updateDriverProfile(driverId, driverProfileRequestDTO);
            responseDTO.setStatusCode(HttpStatus.OK.value());
            responseDTO.setMessage("Driver profile updated successfully.");
            return ResponseEntity.ok().body(responseDTO);
        } catch (UserNotFoundException e) {
            log.error("Exception occurred: {}", e.getMessage());
            return buildErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidDataException e) {
            log.error("InvalidDataException occurred:{}", e);
            return buildErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UserProfileNotFoundException e) {
            log.error("Exception occurred: {}", e.getMessage());
            return buildErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (Exception e) {
            log.error("Exception occurred:{}", e.getMessage());
            return buildErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Exception occurred in driver profile update");
        }
    }

    private ResponseEntity<DriverProfileResponseDTO> buildErrorResponse(HttpStatus httpStatus, String errorMessage) {
        DriverProfileResponseDTO responseDTO = new DriverProfileResponseDTO();
        responseDTO.setMessage(errorMessage);
        responseDTO.setStatusCode(httpStatus.value());
        return ResponseEntity.status(httpStatus).body(responseDTO);
    }

    @PutMapping("/{driverId}/ready")
    public ResponseEntity<String> markDriverReady(@PathVariable Long driverId) {
        try {
            String response = driverProfileService.updateDriverStatus(driverId, Status.AVAILABLE);
            return ResponseEntity.ok(response);
        } catch (UserNotFoundException e) {
            log.error("UserNotFoundException occurred:{}", e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (UserProfileNotActiveException e) {
            log.error("UserProfileNotActiveException occurred:{}", e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to mark driver as ready: " + e.getMessage());
        }
    }

    @PutMapping("/{driverId}/activate")
    public ResponseEntity<String> activateDriverProfileStatus(@PathVariable Long driverId) {
        try {
            String response = driverProfileService.updateDriverStatus(driverId, Status.ACTIVE);
            return ResponseEntity.ok(response);
        } catch (UserNotFoundException e) {
            log.error("UserNotFoundException occurred:{}", e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (UserProfileNotActiveException e) {
            log.error("UserProfileNotActiveException occurred:{}", e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Failed to update driver profile status: " + e.getMessage());
        }
    }

}

