package com.arshad.rideapp.onboarding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RideApplicationDemo {

    public static void main(String[] args) {
        SpringApplication.run(RideApplicationDemo.class, args);
    }

}
