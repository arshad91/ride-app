package com.arshad.rideapp.onboarding.repository;

import com.arshad.rideapp.onboarding.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findById(Long id);

    Optional<User> findByEmail(String email);

    Optional<List<User>> findByName(String name);
}
