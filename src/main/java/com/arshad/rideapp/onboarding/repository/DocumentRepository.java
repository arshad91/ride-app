package com.arshad.rideapp.onboarding.repository;

import com.arshad.rideapp.onboarding.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {
}
