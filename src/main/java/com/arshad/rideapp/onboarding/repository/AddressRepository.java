package com.arshad.rideapp.onboarding.repository;

import com.arshad.rideapp.onboarding.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
}
