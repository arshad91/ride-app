package com.arshad.rideapp.onboarding.constants;

import java.util.regex.Pattern;

public class Constants {
    // The number should be of 10 digits.
    public static final Pattern mobileRegex = Pattern.compile("^\\d{10}$");
    public static final Pattern emailRegex = Pattern.compile("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$", Pattern.CASE_INSENSITIVE);
}
