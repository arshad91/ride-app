package com.arshad.rideapp.onboarding.service;


import com.arshad.rideapp.onboarding.dto.request.AddressRequestDTO;
import com.arshad.rideapp.onboarding.dto.request.UserRegistrationDTO;
import com.arshad.rideapp.onboarding.dto.response.UserResponseDTO;
import com.arshad.rideapp.onboarding.entity.Address;
import com.arshad.rideapp.onboarding.entity.User;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.mapper.AddressMapper;
import com.arshad.rideapp.onboarding.mapper.UserMapper;
import com.arshad.rideapp.onboarding.repository.AddressRepository;
import com.arshad.rideapp.onboarding.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private AddressMapper addressMapper;

    @Mock
    private AddressRepository addressRepository;


    @InjectMocks
    private UserService userService;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRegisterUser() throws InvalidDataException, UserAlreadyExistsException {
        // Create a test UserRegistrationDTO
        UserRegistrationDTO registrationDTO = createUserRegistrationDTO();
        UserResponseDTO UserResponseDTO = createUserResponseDTO();
        User testUser = new User();
        testUser.setName("arshad");
        testUser.setEmail("arshad@gmail.com");
        testUser.setPhoneNumber("1234567890");

        Address address = new Address();

        Mockito.when(userRepository.save(Mockito.any())).thenReturn(testUser);
        Mockito.when(userMapper.mapUserDTOToDomain(Mockito.any())).thenReturn(testUser);
        Mockito.when(userMapper.mapUserDomainToDTO(Mockito.any())).thenReturn(UserResponseDTO);
        Mockito.when(addressMapper.mapAddressRequestDTOToDomain(Mockito.any(), Mockito.any())).thenReturn(address);
        Mockito.when(addressRepository.save(Mockito.any())).thenReturn(address);

        // Perform the registration
        UserResponseDTO = userService.registerUser(registrationDTO);

        assertNotNull(UserResponseDTO);
        assertEquals("arshad", UserResponseDTO.getName());
        assertEquals("arshad@gmail.com", UserResponseDTO.getEmail());
        assertEquals("1234567890", UserResponseDTO.getPhoneNumber());
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testRegisterUserEmailAlreadyExists() throws InvalidDataException, UserAlreadyExistsException {
        UserRegistrationDTO registrationDTO = createUserRegistrationDTO();

        Mockito.when(addressMapper.mapAddressRequestDTOToDomain(Mockito.any(), Mockito.any())).thenReturn(new Address());
        Optional<User> optionalUser = Optional.of(new User());
        Mockito.when(userRepository.findByEmail("arshad@gmail.com")).thenReturn(optionalUser);
        userService.registerUser(registrationDTO);
    }

    private UserRegistrationDTO createUserRegistrationDTO() {
        UserRegistrationDTO registrationDTO = new UserRegistrationDTO();
        registrationDTO.setName("arshad");
        registrationDTO.setEmail("arshad@gmail.com");
        registrationDTO.setPhoneNumber("1234567890");
        registrationDTO.setAddress(new AddressRequestDTO("1", "street1", "444444", "bengaluru", "karnataka", "india"));
        return registrationDTO;
    }

    private UserResponseDTO createUserResponseDTO() {
        UserResponseDTO UserResponseDTO = new UserResponseDTO();
        UserResponseDTO.setName("arshad");
        UserResponseDTO.setEmail("arshad@gmail.com");
        UserResponseDTO.setPhoneNumber("1234567890");
        return UserResponseDTO;
    }

    private UserRegistrationDTO createInvalidUserRegistrationDTO() {
        UserRegistrationDTO registrationDTO = new UserRegistrationDTO();
        registrationDTO.setName("arshad");
        registrationDTO.setPhoneNumber("1234567890");
        return registrationDTO;
    }
}