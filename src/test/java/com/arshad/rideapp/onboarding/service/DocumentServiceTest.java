package com.arshad.rideapp.onboarding.service;

import com.arshad.rideapp.onboarding.dto.response.DocumentResponseDTO;
import com.arshad.rideapp.onboarding.entity.Document;
import com.arshad.rideapp.onboarding.entity.User;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.mapper.DocumentMapper;
import com.arshad.rideapp.onboarding.repository.DocumentRepository;

import com.arshad.rideapp.onboarding.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@ExtendWith(MockitoExtension.class)
public class DocumentServiceTest {

    @InjectMocks
    private DocumentService documentService;

    @Mock
    private DocumentRepository documentRepository;

    @Mock
    private DocumentMapper documentMapper;

    @Mock
    private UserRepository userRepository;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testUploadDocument() throws IOException, InvalidDataException, UserNotFoundException {
        MockMultipartFile file = new MockMultipartFile(
                "file", "testfile.txt", "text/plain", "test!".getBytes());

        Document testDocument = createTestDocument();
        DocumentResponseDTO responseDocument = getDocumentResponseDTO();
        Mockito.when(documentMapper.mapDocumentDTOToDomain(Mockito.any(), Mockito.any())).thenReturn(testDocument);
        Mockito.when(documentRepository.save(Mockito.any())).thenReturn(testDocument);
        Mockito.when(documentMapper.mapDocumentDomainToDTO(Mockito.any())).thenReturn(responseDocument);
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.of(new User()));
        DocumentResponseDTO responseDTO = documentService.uploadDocument(1L, file);
        assertNotNull(responseDTO);
        assertEquals("testfile.txt", responseDTO.getFileName());
        assertEquals("text/plain", responseDTO.getContentType());

    }

    @Test(expected = UserNotFoundException.class)
    public void testUploadDocumentDriverNotFoundException() throws IOException, InvalidDataException, UserNotFoundException {
        MockMultipartFile file = new MockMultipartFile(
                "file", "testfile.txt", "text/plain", "Hello, World!".getBytes());
        documentService.uploadDocument(1L, file);
    }

    private Document createTestDocument() {
        Document document = new Document();
        document.setId(1L);
        document.setFileName("testfile.txt");
        document.setContentType("text/plain");
        document.setUploadDate(LocalDateTime.now());
        return document;
    }

    private DocumentResponseDTO getDocumentResponseDTO() {
        DocumentResponseDTO responseDTO = new DocumentResponseDTO();
        responseDTO.setId(1L);
        responseDTO.setFileName("testfile.txt");
        responseDTO.setContentType("text/plain");
        return responseDTO;
    }
}
