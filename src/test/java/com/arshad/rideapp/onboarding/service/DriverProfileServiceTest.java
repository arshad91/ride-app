package com.arshad.rideapp.onboarding.service;

import com.arshad.rideapp.onboarding.dto.request.DriverProfileRequestDTO;
import com.arshad.rideapp.onboarding.dto.response.DriverProfileResponseDTO;
import com.arshad.rideapp.onboarding.entity.DriverProfile;
import com.arshad.rideapp.onboarding.entity.User;
import com.arshad.rideapp.onboarding.enums.Status;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.exceptions.UserProfileNotActiveException;
import com.arshad.rideapp.onboarding.exceptions.UserProfileNotFoundException;
import com.arshad.rideapp.onboarding.mapper.DriverProfileMapper;
import com.arshad.rideapp.onboarding.repository.DriverProfileRepository;
import com.arshad.rideapp.onboarding.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(MockitoJUnitRunner.class)
public class DriverProfileServiceTest {

    @InjectMocks
    private DriverProfileService driverProfileService;

    @Mock
    private DriverProfileRepository driverProfileRepository;

    @Mock
    private DriverProfileMapper driverProfileMapper;

    @Mock
    private UserRepository userRepository;

    @Before
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testCreateDriverProfile() throws UserNotFoundException, InvalidDataException, UserAlreadyExistsException {
        DriverProfileRequestDTO driverProfileRequestDTO = createDriverProfileRequestDTO();

        DriverProfile testProfile = getDriverProfile();
        DriverProfileResponseDTO driverProfileResponseDTO = getDriverProfileResponseDTO();

        Mockito.when(driverProfileRepository.save(Mockito.any())).thenReturn(testProfile);
        Mockito.when(driverProfileMapper.mapDriverProfileDTOToDomain(Mockito.any(), Mockito.any())).thenReturn(testProfile);
        Mockito.when(driverProfileMapper.mapDriverProfileDomainToDTO(Mockito.any())).thenReturn(driverProfileResponseDTO);
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(new User(1L, "arshad", "arshad@gmail.com", "pwd123", "1234567890", null, LocalDateTime.now(), LocalDateTime.now())));

        DriverProfileResponseDTO responseDTO = driverProfileService.createDriverProfile(1L, driverProfileRequestDTO);

        assertNotNull(responseDTO);
        assertEquals("SUV", responseDTO.getVehicleType());
        assertEquals("KR 06 D 1243", responseDTO.getLicensePlate());
    }

    @Test
    public void testCreateDriverProfileInvalidVehicle() throws UserNotFoundException, InvalidDataException, UserAlreadyExistsException {
        DriverProfileRequestDTO driverProfileRequestDTO = new DriverProfileRequestDTO("", "");

        Exception exception = assertThrows(InvalidDataException.class, () -> driverProfileService.createDriverProfile(1L, driverProfileRequestDTO));
        String expectedMessage = "VehicleType  is invalid";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testUpdateDriverProfile() throws UserNotFoundException, InvalidDataException, UserAlreadyExistsException, UserProfileNotFoundException {
        DriverProfileRequestDTO driverProfileRequestDTO = createDriverProfileRequestDTO();

        DriverProfile testProfile = getDriverProfile();
        DriverProfileResponseDTO driverProfileResponseDTO = getDriverProfileResponseDTO();

        Mockito.when(driverProfileRepository.save(Mockito.any())).thenReturn(testProfile);
        Mockito.when(driverProfileRepository.findByUserId(Mockito.any())).thenReturn(Optional.ofNullable(new DriverProfile(1L, new User(), "SUV", "KR 06 D 1243", new Double(0), null, null, null)));
        Mockito.when(driverProfileMapper.mapDriverProfileDTOToDomain(Mockito.any(), Mockito.any())).thenReturn(testProfile);
        Mockito.when(driverProfileMapper.mapDriverProfileDomainToDTO(Mockito.any())).thenReturn(driverProfileResponseDTO);
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(new User(1L, "arshad", "arshad@gmail.com", "pwd123", "1234567890", null, LocalDateTime.now(), LocalDateTime.now())));

        DriverProfileResponseDTO responseDTO = driverProfileService.updateDriverProfile(1L, driverProfileRequestDTO);
        assertNotNull(responseDTO);
        assertEquals("SUV", responseDTO.getVehicleType());
        assertEquals("KR 06 D 1243", responseDTO.getLicensePlate());
    }

    @Test
    public void testUpdateDriverProfileDoesNotExists() throws UserNotFoundException, InvalidDataException, UserAlreadyExistsException, UserProfileNotFoundException {
        DriverProfileRequestDTO driverProfileRequestDTO = createDriverProfileRequestDTO();
        Mockito.when(userRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(new User(1L, "arshad", "arshad@gmail.com", "pwd123", "1234567890", null, LocalDateTime.now(), LocalDateTime.now())));
        Exception exception = assertThrows(UserProfileNotFoundException.class, () -> driverProfileService.updateDriverProfile(1L, driverProfileRequestDTO));
        String expectedMessage = "User profile does not exists";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testDriverMarkAvailable() throws UserNotFoundException, UserProfileNotActiveException {
        DriverProfile driverProfile = getDriverProfile();
        Optional<DriverProfile> optionalProfile = Optional.ofNullable(driverProfile);
        Mockito.when(driverProfileRepository.findByUserId(Mockito.any())).thenReturn(optionalProfile);
        Mockito.when(driverProfileRepository.save(Mockito.any())).thenReturn(optionalProfile.get());

        String response = driverProfileService.updateDriverStatus(1L, Status.AVAILABLE);
        assertNotNull(response);
        assertEquals("Driver status AVAILABLE updated successfully", response);
    }

    @Test(expected = UserProfileNotActiveException.class)
    public void testUpdateDriverStatusForPendingStatus() throws UserNotFoundException, UserProfileNotActiveException {
        DriverProfile driverProfile = getDriverProfile();
        driverProfile.setDriverStatus(Status.PENDING.name());
        Optional<DriverProfile> optionalProfile = Optional.ofNullable(driverProfile);
        Mockito.when(driverProfileRepository.findByUserId(Mockito.any())).thenReturn(optionalProfile);

        String response = driverProfileService.updateDriverStatus(1L, Status.AVAILABLE);
    }

    private DriverProfileResponseDTO getDriverProfileResponseDTO() {
        DriverProfileResponseDTO responseDTO = new DriverProfileResponseDTO();
        responseDTO.setVehicleType("SUV");
        responseDTO.setLicensePlate("KR 06 D 1243");
        return responseDTO;
    }

    private DriverProfile getDriverProfile() {
        DriverProfile driverProfile = new DriverProfile();
        User user =new User();
        user.setId(1L);
        driverProfile.setUser(user);
        driverProfile.setVehicleType("SUV");
        driverProfile.setLicensePlate("KR 06 D 1243");
        driverProfile.setDriverStatus(Status.ACTIVE.name());
        return driverProfile;

    }

    private DriverProfileRequestDTO createDriverProfileRequestDTO() {
        DriverProfileRequestDTO profileDTO = new DriverProfileRequestDTO();
        profileDTO.setVehicleType("SUV");
        profileDTO.setLicensePlate("KR 06 D 1243");
        return profileDTO;
    }
}

