package com.arshad.rideapp.onboarding.controller;

import com.arshad.rideapp.onboarding.exceptions.UserProfileNotActiveException;
import com.arshad.rideapp.onboarding.exceptions.UserProfileNotFoundException;
import com.arshad.rideapp.onboarding.service.DriverProfileService;
import com.arshad.rideapp.onboarding.dto.request.DriverProfileRequestDTO;
import com.arshad.rideapp.onboarding.dto.response.DriverProfileResponseDTO;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.UserNotFoundException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class DriverProfileControllerTest {

    @InjectMocks
    private DriverProfileController driverProfileController;

    @Mock
    private DriverProfileService driverProfileService;

    @Test
    public void testCreateDriverProfile() throws UserNotFoundException, InvalidDataException, UserAlreadyExistsException {
        // Create a test DriverProfileDTO
        DriverProfileRequestDTO profileDTO = createDriverProfileRequestDTO();

        DriverProfileResponseDTO responseDTO = new DriverProfileResponseDTO();
        responseDTO.setVehicleType("SUV");
        responseDTO.setLicensePlate("KR 06 D 1243");
        Mockito.when(driverProfileService.createDriverProfile(Mockito.any(Long.class), Mockito.any())).thenReturn(responseDTO);

        // Perform profile creation request
        ResponseEntity<DriverProfileResponseDTO> response = driverProfileController.createDriverProfile(1L, profileDTO);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("SUV", response.getBody().getVehicleType());
        assertEquals("KR 06 D 1243", response.getBody().getLicensePlate());
    }

    @Test
    public void testUpdateDriverProfile() throws UserNotFoundException, InvalidDataException, UserAlreadyExistsException, UserProfileNotFoundException {
        // Create a test DriverProfileDTO
        DriverProfileRequestDTO profileDTO = createDriverProfileRequestDTO();

        DriverProfileResponseDTO responseDTO = new DriverProfileResponseDTO();
        responseDTO.setVehicleType("SUV");
        responseDTO.setLicensePlate("KR 06 D 1243");
        Mockito.when(driverProfileService.updateDriverProfile(Mockito.any(Long.class), Mockito.any())).thenReturn(responseDTO);

        // Perform profile creation request
        ResponseEntity<DriverProfileResponseDTO> response = driverProfileController.updateDriverProfile(1L, profileDTO);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("SUV", response.getBody().getVehicleType());
        assertEquals("KR 06 D 1243", response.getBody().getLicensePlate());
    }

    @Test
    public void testMarkDriverReadyStatus() throws UserNotFoundException, UserProfileNotActiveException {
        Mockito.when(driverProfileService.updateDriverStatus(Mockito.any(), Mockito.any())).thenReturn("Driver is ready to take a ride.");

        ResponseEntity<?> response = driverProfileController.markDriverReady(1L);

        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Driver is ready to take a ride.", response.getBody());
    }

    @Test
    public void testActivateDriverProfileStatus() throws UserNotFoundException, UserProfileNotActiveException {
        Mockito.when(driverProfileService.updateDriverStatus(Mockito.any(), Mockito.any())).thenReturn("Driver status ACTIVE updated successfully");

        ResponseEntity<?> response = driverProfileController.activateDriverProfileStatus(1L);

        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Driver status ACTIVE updated successfully", response.getBody());
    }


    private DriverProfileRequestDTO createDriverProfileRequestDTO() {
        DriverProfileRequestDTO profileDTO = new DriverProfileRequestDTO();
        profileDTO.setVehicleType("SUV");
        profileDTO.setLicensePlate("KR 06 D 1243");
        return profileDTO;
    }
}
