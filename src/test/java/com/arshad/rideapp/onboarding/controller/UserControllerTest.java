package com.arshad.rideapp.onboarding.controller;

import com.arshad.rideapp.onboarding.dto.request.UserRegistrationDTO;
import com.arshad.rideapp.onboarding.dto.response.UserResponseDTO;
import com.arshad.rideapp.onboarding.exceptions.UserAlreadyExistsException;
import com.arshad.rideapp.onboarding.exceptions.InvalidDataException;
import com.arshad.rideapp.onboarding.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRegisterUser() throws InvalidDataException, UserAlreadyExistsException {
        UserRegistrationDTO registrationDTO = createUserRegistrationDTO();
        UserResponseDTO responseDTO = createUserResponseDTO();

        Mockito.when(userService.registerUser(Mockito.any(UserRegistrationDTO.class))).thenReturn(responseDTO);

        ResponseEntity<UserResponseDTO> response = userController.registerUser(registrationDTO);

        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(1L, response.getBody().getId().longValue());
        assertEquals("arshad", response.getBody().getName());
        assertEquals("arshad@gmail.com", response.getBody().getEmail());
        assertEquals("1234567890", response.getBody().getPhoneNumber());
        assertEquals("User registered successfully.", response.getBody().getMessage());
    }


    @Test
    public void testRegisterUserInvalidData() throws InvalidDataException {
        UserRegistrationDTO registrationInvalidDTO = createInvalidUserRegistrationDTO();

    }

    private UserRegistrationDTO createUserRegistrationDTO() {
        UserRegistrationDTO registrationDTO = new UserRegistrationDTO();
        registrationDTO.setName("arshad");
        registrationDTO.setEmail("arshad@gmail.com");
        registrationDTO.setPhoneNumber("1234567890");
        return registrationDTO;
    }

    private UserResponseDTO createUserResponseDTO() {
        UserResponseDTO responseDTO = new UserResponseDTO();
        responseDTO.setId(1L);
        responseDTO.setStatusCode(HttpStatus.CREATED.value());
        responseDTO.setMessage("User registered successfully.");
        responseDTO.setName("arshad");
        responseDTO.setEmail("arshad@gmail.com");
        responseDTO.setPhoneNumber("1234567890");
        return responseDTO;
    }

    private UserRegistrationDTO createInvalidUserRegistrationDTO() {
        UserRegistrationDTO registrationDTO = new UserRegistrationDTO();
        registrationDTO.setName("arshad");
        registrationDTO.setPhoneNumber("1234567890");
        return registrationDTO;
    }

    // Add more test cases for UserController as needed
}
