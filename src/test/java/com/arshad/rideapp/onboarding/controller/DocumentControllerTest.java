package com.arshad.rideapp.onboarding.controller;

import com.arshad.rideapp.onboarding.service.DocumentService;
import com.arshad.rideapp.onboarding.dto.response.DocumentResponseDTO;
import com.arshad.rideapp.onboarding.entity.Document;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@ExtendWith(MockitoExtension.class)
public class DocumentControllerTest {

    @InjectMocks
    private DocumentController documentController;

    @Mock
    private DocumentService documentService;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testUploadDocument() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
                "file", "testfile.txt", "text/plain", "Hello, World!".getBytes());

        DocumentResponseDTO uploadedDocument = getDocumentResponseDTO();

        Mockito.when(documentService.uploadDocument(Mockito.any(), Mockito.any())).thenReturn(uploadedDocument);
        ResponseEntity<DocumentResponseDTO> response = documentController.uploadDocument(1L, file);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(1, response.getBody().getId().intValue());
        assertEquals("testfile.txt", response.getBody().getFileName());
        assertEquals("text/plain", response.getBody().getContentType());
    }



    @Test
    public void testUploadDocumentError() throws Exception {
        MockMultipartFile file = new MockMultipartFile(
                "file", "testfile.txt", "text/plain", "Hello, World!".getBytes());

        Mockito.when(documentService.uploadDocument(Mockito.any(), Mockito.any())).thenThrow(IOException.class);

        ResponseEntity<DocumentResponseDTO> response = documentController.uploadDocument(1L, file);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
    }

    private Document createTestDocument() {
        Document document = new Document();
        document.setId(1L);
        document.setFileName("testfile.txt");
        document.setContentType("text/plain");
        document.setUploadDate(LocalDateTime.now());
        return document;
    }

    private DocumentResponseDTO getDocumentResponseDTO() {
        DocumentResponseDTO responseDTO = new DocumentResponseDTO();
        responseDTO.setId(1L);
        responseDTO.setFileName("testfile.txt");
        responseDTO.setContentType("text/plain");
        return responseDTO;
    }
}
